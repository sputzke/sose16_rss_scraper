#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: scraper_bbc.pl
#
#        USAGE: ./scraper_bbc.pl  
#
#  DESCRIPTION: scrapes the rss feed of the bbc
#
#      OPTIONS: ---
# REQUIREMENTS: XML::LibXML;
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Simon Putzke (nerdbeere), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 17.06.2016 20:44:27
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use feature 'say';
use Data::Printer;
use XML::LibXML;

my $parser = XML::LibXML->new();

my $filename = "rss.xml";
my $doc = $parser->parse_file($filename);
my @results;
foreach my $item ($doc->findnodes('/rss/channel/item')) {
    my $title = $item->findnodes('./title');
#    say "Title: " . $title->to_literal;
    my $title_string        = $title->to_literal->value();
    my $description         = $item->findnodes('./description');
    my $description_string  = $description->to_literal->value();
    my $date                = $item->findnodes('./pubDate');
    my $date_string         = $date->to_literal->value();
    my $link                = $item->findnodes('./link');
    my $link_string         = $link->to_literal->value();

    my %item_hash = (
                        title       => $title_string,
                        description => $description_string,
                        date        => $date_string,
                        link        => $link_string,

                    );

    push @results, \%item_hash;
}
p @results;
